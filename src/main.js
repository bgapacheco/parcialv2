import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@/assets/css/main.css'
import '@/assets/logo.png'

import * as VueGoogleMaps from 'vue2-google-maps'
Vue.config.productionTip = false
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyClx_QazXqF8vbjS9YYA5kRWrgyMwNgee8'
  },
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
